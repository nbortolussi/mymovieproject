# My Movie Project

This project is an infinite scrolling movie crawler. It displays all movies released so far this year sorted by highest rated movies descending order.

## API Usage

	- Used The Movie Database discovery API.
	
## Third Party SDKs Used

	- Used several third party SDKs that I like to use in my own projects.
	- Retrofit for making REST calls.
	- OKHTTP as an HTTP client.
	- MaterialProgressBar for beautiful loading.
	- Fresco for image loading / caching.