package com.mymovieproject;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.ArrayList;


public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<MovieResponse.Movie> data;

    public MovieAdapter() {
        this.data = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MovieViewHolder movieViewHolder = (MovieViewHolder) viewHolder;
        MovieResponse.Movie movie = data.get(position);
        movieViewHolder.description.setText(movie.getOverview());
        movieViewHolder.rating.setText(Double.toString(movie.getRating()));
        movieViewHolder.release.setText(movie.getReleaseDate());
        movieViewHolder.title.setText(movie.getTitle());
        movieViewHolder.poster.setImageURI(Uri.parse(movie.getPosterPath()));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_movie, viewGroup, false);
        return new MovieViewHolder(itemView);
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder {
        protected SimpleDraweeView poster;
        protected TextView release;
        protected TextView title;
        protected TextView description;
        protected TextView rating;

        public MovieViewHolder(View v) {
            super(v);
            poster = (SimpleDraweeView) v.findViewById(R.id.poster);
            title = (TextView) v.findViewById(R.id.title);
            description = (TextView) v.findViewById(R.id.description);
            rating = (TextView) v.findViewById(R.id.rating);
            release = (TextView) v.findViewById(R.id.release);
        }
    }

    public void addData(ArrayList<MovieResponse.Movie> data) {
        this.data.addAll(data);
        notifyItemRangeChanged(this.data.size() - data.size(), data.size());
    }

    public void clearData() {
        this.data.clear();
        notifyDataSetChanged();
    }
}