package com.mymovieproject;

import java.util.ArrayList;

/**
 * Created by nbortolussi on 5/26/16.
 */
public class MovieResponse {
    private int page;
    private int total_pages;
    private int total_results;
    private ArrayList<Movie> results;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return total_pages;
    }

    public void setTotalPages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotalResults() {
        return total_results;
    }

    public void setTotalResults(int total_results) {
        this.total_results = total_results;
    }

    public ArrayList<Movie> getMovies() {
        return results;
    }

    public void setMovies(ArrayList<Movie> results) {
        this.results = results;
    }

    public static class Movie {
        private static final String image_prefix = "https://image.tmdb.org/t/p/w300";
        private String poster_path;
        private String overview;
        private String release_date;
        private String title;
        private double vote_average;

        public String getPosterPath() {
            return image_prefix + poster_path;
        }

        public void setPosterPath(String poster_path) {
            this.poster_path = poster_path;
        }

        public String getOverview() {
            return overview;
        }

        public void setOverview(String overview) {
            this.overview = overview;
        }

        public String getReleaseDate() {
            return release_date;
        }

        public void setReleaseDate(String release_date) {
            this.release_date = release_date;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public double getRating() {
            return vote_average;
        }

        public void setRating(double vote_average) {
            this.vote_average = vote_average;
        }
    }
}
