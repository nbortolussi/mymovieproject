package com.mymovieproject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class MainActivity extends AppCompatActivity {

    private GetTopRatedMovies getTopRatedMovies;
    private MaterialProgressBar loadingprogress;
    private MovieAdapter movieAdapter;
    private int page = 1;
    private boolean hitEndOfList;
    private final Semaphore grabMovies = new Semaphore(1);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupActivity();
        getTopRatedMovies();
    }

    public void setupActivity() {
        loadingprogress = (MaterialProgressBar) findViewById(R.id.loadingprogress);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.movierv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        movieAdapter = new MovieAdapter();
        recyclerView.setAdapter(movieAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!hitEndOfList && dy > 0) {
                    int lastVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                    if (lastVisibleItemPosition == recyclerView.getAdapter().getItemCount() - 1) {
                        getTopRatedMovies();
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (getTopRatedMovies != null)
            getTopRatedMovies.cancel(true);
    }

    public void getTopRatedMovies() {
        if (grabMovies.tryAcquire()) {
            getTopRatedMovies = new GetTopRatedMovies();
            getTopRatedMovies.execute();
        }
    }

    public class GetTopRatedMovies extends AsyncTask<Void, Void, Boolean> {

        private MovieResponse movieResponse;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingprogress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Void... voided) {
            try {
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                final String releaseGte = "2016-01-01";
                final String releaseLte = simpleDateFormat.format(new Date());
                final String sortBy = "vote_average.desc";

                movieResponse = RestClient.getMovieInterface().getTopRatedMoviesSoFar(BuildConfig.api_key, page, releaseGte, releaseLte, sortBy).execute().body();
                if (page == movieResponse.getTotalPages()) {
                    hitEndOfList = true;
                    return true;
                }
                page++;
                return true;
            } catch (IOException e) {
                Log.e(getClass().getName(), "Oops, our call to get movies failed.", e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success)
                addMovieResponseToAdapter(movieResponse);
            else
                Toast.makeText(MainActivity.this, getString(R.string.fail), Toast.LENGTH_SHORT).show();

            loadingprogress.setVisibility(View.GONE);
            grabMovies.release();
        }
    }

    public void addMovieResponseToAdapter(MovieResponse movieResponse) {
        if (movieResponse != null && movieResponse.getMovies() != null)
            movieAdapter.addData(movieResponse.getMovies());
    }
}
