package com.mymovieproject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nbortolussi on 5/20/16.
 */
public class RestClient {

    private static MovieInterface movieInterface;

    public static MovieInterface getMovieInterface() {
        if (movieInterface == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.themoviedb.org/")
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            movieInterface = retrofit.create(MovieInterface.class);
        }

        return movieInterface;
    }

    public interface MovieInterface {

        @GET("3/discover/movie")
        Call<MovieResponse> getTopRatedMoviesSoFar(@Query("api_key") String api_key, @Query("page") int page, @Query("primary_release_date.gte") String releaseGte, @Query("primary_release_date.lte") String releaseLte, @Query("sort_by") String sortBy);
    }
}
